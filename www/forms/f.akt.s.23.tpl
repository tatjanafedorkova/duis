﻿<body class="frame_1">
<?= $oFormPop -> getFormHeader(); ?>
<h1><?=$title;?></h1>
<div align=center><?= $oFormPop -> getMessage(); ?></div>
<?  if($catalog == KL_CALCULALATION ) { ?>

<table cellpadding="5" cellspacing="1" border="0" width="100%">
<tr>
    <td class="table_cell_c" width="40%"><?= $oFormPop -> getElementLabel('calc_group'); ?></td>
    <td class="table_cell_2" width="60%"><?= $oFormPop -> getElementHtml('calc_group'); ?></td>    
</tr>
</table>
<? } ?>

<?  if($catalog == KL_MATERIALS ) { ?>

    <table cellpadding="5" cellspacing="1" border="0" width="100%">
    <tr>
        <td class="table_cell_c" width="40%"><?= $oFormPop -> getElementLabel('mat_group'); ?></td>
        <td class="table_cell_2" width="60%"><?= $oFormPop -> getElementHtml('mat_group'); ?></td>    
    </tr>
    <tr>
        <td class="table_cell_c" width="40%"><?= $oFormPop -> getElementLabel('mat_sub_group'); ?></td>
        <td class="table_cell_2" width="60%"><?= $oFormPop -> getElementHtml('mat_sub_group'); ?></td>    
    </tr>
    </table>
    <hr>
    <table cellpadding="5" cellspacing="1" border="0" width="100%">
        <tr>
            <td class="table_cell_c" width="40%"><?= $oFormPop -> getElementLabel('category'); ?></td>
            <td class="table_cell_2" width="30%"><?= $oFormPop -> getElementHtml('category'); ?></td> 
            <td class="table_cell_2" width="30%"><?= $oFormPop -> getElementHtml('search'); ?></td>   
        </tr>       
    </table>
    <? } ?>

    <hr>
    <table cellpadding="5" cellspacing="1" border="0" width="100%">
        <tr>
            <td class="table_cell_c" width="40%"><?= $oFormPop -> getElementLabel('text_part'); ?></td>
            <td class="table_cell_2" width="30%"><?= $oFormPop -> getElementHtml('text_part'); ?></td> 
            <td class="table_cell_2" width="30%"><?= $oFormPop -> getElementHtml('search_by_text'); ?></td>   
        </tr>       
    </table>

<table cellpadding="5" cellspacing="1" border="0" width="100%">
<tr>
    <td><?= $oFormPop -> getElementHtml('scope'); ?></td>
</tr>

</table>
<table cellpadding="5" cellspacing="1" border="0" width="100%">
    <tr>
        <td class="table_cell_c" width="20%"><?= $oFormPop -> getElementLabel('work_type'); ?></td>
        <td class="table_cell_2" width="80%"><?= $oFormPop -> getElementHtml('work_type'); ?></td>    
    </tr>
    <?  if($catalog == KL_CALCULALATION ) { ?>
    <tr>
        <td class="table_cell_c" width="20%"><?= $oFormPop -> getElementLabel('work_type_text'); ?></td>
        <td class="table_cell_2" width="80%"><?= $oFormPop -> getElementHtml('work_type_text'); ?></td>    
    </tr>
    <? } ?>
</table>

<table cellpadding="5" cellspacing="0" border="0" align="center">
    <tr>
      <? if(!$isReadonly)  {?>
        <td><?= $oFormPop -> getElementHtml('save'); ?></td>
      <? } ?>
        <td><?=$oFormPop->getElementHtml('close');?></td>
    </tr>
</table>
<?= $oFormPop->getFormBottom(); ?>
</body>