<body class="print">

<table cellpadding="0" cellspacing="0" class="print_table_portrait" >
<? if ($isMaterialPrice != 1) { ?>
  <tr>
    <td  colspan="2" align="right"><h4><?= text::toUpper(text::get('PROJECT')); ?></h4></td>
  </tr>
<? } ?>
  <tr>
    <td  colspan="2" class="print_header_centered" align="center"><?= (($isMaterialPrice == 1) ? text::get('TRASE_ACT_HEADER1') :  text::get('TRASE_ACT_HEADER4')); ?></td>
  </tr>
  <tr>
     <td colspan="2">&nbsp;</td>
  </tr>
  <tr>     
        <td></td>
    <td align="right"><b><?=text::get('NUMBER_SIMBOL').'. '.$act['NUMURS']; ?></b></td>
  </tr>
  <tr>
     <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
     <td><?= text::get('SUPORT_ACT_HEADER5'); ?></td>
<?
	if(isset($act['RAKT_IR_OBJEKTS_PABEIGTS']) && $act['RAKT_IR_OBJEKTS_PABEIGTS'] == 0)
	{
		if(isset($act['PLAN']) && $act['PLAN'] == 1)
		{
    ?>
          <td align="right"><b><?= $act['RAKT_NUM_POSTFIX'].' / '. text::get('STARPAKTS') ; ?></b></td>
    <?
		}
		else
		{
    ?>
          <td align="right"><b><?= text::get('STARPAKTS') ; ?></b></td>
    <?
		}
	}
	else
	{
  ?>
    <td>&nbsp;</td>
  <?	
	}
?>
          
  </tr>
  <tr>
     <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td><?= text::get('REPORT_WORK_OFFER').':'; ?></td>
    <td><?= text::get('SINGLE_SOURCE_OF_FOUNDS'); ?></td>
  </tr>
  <tr>
    <td><b><?= text::get('ST_COMPANY_NAME').', '.text::get('REGISTRATION_SIMBOL').'. '.text::get('NUMBER_SIMBOL').': '.text::get('ST_REG_NR'); ?></b></td>
    <td><b><?= $act['WRITE_OFF_ACCOUNT']; ?></b></td>
  </tr>
  <tr>
    <td><?= text::get('ED_IECIKNIS').':'; ?></td>
    <td><?= text::get('REPORT_WORK_DONE'); ?></td>
  </tr>
  <tr>
    <td><b><?= $act['KEDI_SECTION']; ?></b></td>
    <td><b><?= $act['WORKER'].', '.text::get('REGISTRATION_SIMBOL').'. '.text::get('NUMBER_SIMBOL').': '.$act['KWOI_KODS']; ?></b></td>
  </tr>
  <tr>
    <td><?= text::get('WORKS_AND_TERITORY'); ?></td>
    <td><?= text::get('WORK_DONE_AGREEMENT_NO').': '.$act['RAKT_VV_NUMBER']; ?></td>
  </tr>
  
  <tr>
    <td><b><?= $act['KEDI_NOSAUKUMS']; ?></b></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
     <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
     <td colspan="2"><?= $act['WORK_TITLE']; ?></td>
  </tr>
  <tr>
     <td colspan="2">&nbsp;</td>
  </tr>
</table>
<table cellpadding="0" cellspacing="0" class="print_table_portrait">

    <tr>
        <td width="15%" ><b><i><?= text::get('SINGLE_ACT_TYPE').': '; ?></i></b></td>
        <td width="35%" ><?= ' '.$act['TYPE']; ?></td>
        <td width="15%" ><b><i><?= text::get('SINGLE_OBJECT').': '; ?></i></b></td>
        <td width="35%" ><?= ' '.$act['OBJECT']; ?></td>

    </tr>
    <tr>
        <td ><b><i><?= text::get('SINGLE_VOLTAGE').': '; ?></i></b></td>
        <td><?= ' '.$act['SPRIEGUMS']; ?></td>
        <td><b><i><?= text::get('EXPORT_MAIN_DESIGNATION').': '; ?></i></b></td>
        <td ><?= ' '.$act['RAKT_OPERATIVAS_APZIM']; ?></td>
    </tr>


    <tr>
        <td><b><i><?= text::get('COMMENT').': '; ?></i></b></td>
        <td><?= ' '.$act['RAKT_PIEZIMES']; ?></td>
                <td><b><i><?= text::get('TO_ID').': '; ?></i></b></td>
        	<td><?= ' '.$act['RAKT_TO_ID']; ?></td>

    </tr>
</table>
<br />
<div class="print_header"><?= text::get('TAB_WORKS'); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >

		<tr>
            <td width="12%" class="print_table_header"><?= text::get('CHIPHER'); ?></td>
            <td width="12%" class="print_table_header"><?= text::get('WORK_APPROVE_DATE'); ?></td>
			      <td width="50%" class="print_table_header"><?= text::get('EXPORT_CALCULATION_NAME'); ?></td>
            <td width="12%" class="print_table_header"><?=text::get('UNIT_OF_MEASURE');?></td>
            <td width="12%" class="print_table_header"><?=text::get('PRICE');?></td>
            <td width="12%" class="print_table_header"><?=text::get('AMOUNT');?></td>
            <td width="12%" class="print_table_header"><?= text::get('PRICE_TOTAL'); ?></td>
 		</tr>

<?
	if (is_array($aWorkDb) && count($aWorkDb) > 0)
	{
		foreach ($aWorkDb as $i => $work)
		{
?>
			<tr >
                <td align="center" class="print_table_data">&nbsp;<?= $work['DRBI_KKAL_SHIFRS']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $work['DRBI_WORK_APROVE_DATE']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $work['DRBI_KKAL_NOSAUKUMS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $work['DRBI_MERVIENIBA']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $work['DRBI_KKAL_NORMATIVS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $work['DRBI_DAUDZUMS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $work['DRBI_CILVEKSTUNDAS']; ?></td>
			</tr>
<?
		}
  }
?>
        <tr>
             <td align="right" class="print_table_total" colspan="5"><?=text::get('STARPTOTAL');?>:</td>
             <td align="center" class="print_table_total"><?= number_format($workTotal,2,'.',''); ?></td>
        </tr>


</table>

<br />
<div class="print_header"><?= text::get(TAB_MATERIAL); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >

		<tr>
            <td width="12%" class="print_table_header"><?= text::get('CODE'); ?></td>
            <td width="12%" class="print_table_header"><?= text::get('WORK_APPROVE_DATE'); ?></td>
			      <td width="50%" class="print_table_header"><?= text::get('NAME'); ?></td>
            <td width="12%" class="print_table_header"><?=text::get('UNIT_OF_MEASURE');?></td>
            <td width="12%" class="print_table_header"><?=text::get('PRICE');?></td>
            <td width="12%" class="print_table_header"><?=text::get('AMOUNT');?></td>
            <td width="12%" class="print_table_header"><?= text::get('PRICE_TOTAL'); ?></td>
 		</tr>

<?
	if (is_array($aMaterialDb) && count($aMaterialDb) > 0)
	{
		foreach ($aMaterialDb as $i => $material)
		{
?>
			<tr >
                <td align="center" class="print_table_data">&nbsp;<?= $material['MATR_KODS']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $material['MATR_WORK_APROVE_DATE']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $material['MATR_NOSAUKUMS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $material['MATR_MERVIENIBA']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $material['MATR_CENA']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $material['MATR_DAUDZUMS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $material['MATR_CENA_KOPA']; ?></td>
			</tr>
<?
		}
?>

<?
	}
?>
         <tr>
             <td align="right" class="print_table_total" colspan="5"><?=text::get('STARPTOTAL');?>:</td>
             <td align="center" class="print_table_total"><?= number_format($materialTotal,2,'.',''); ?></td>
        </tr>
         <tr>
             <td align="right" class="print_table_total" colspan="5"><?=text::get('TOTAL');?>:</td>
             <td align="center" class="print_table_total"><?= number_format($workTotal+ $materialTotal,2,'.','' ); ?></td>
        </tr>

</table>
<br />
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >
  <tr>
    <td width="30%"><?= text::get('ACT_CREATER').': '.$act['AUTORS']; ?></td>
    <td width="30%"></td>
    <td width="30%"></td>
  </tr>
  <tr>
    <td width="30%"><?= text::get('DATE').': '.date('d.m.Y'); ?></td>
    <td width="30%"></td>
    <td width="30%"></td>
  </tr>
   <tr>
    <td width="30%">&nbsp;</td>
    <td width="30%">&nbsp;</td>
    <td width="30%">&nbsp;</td>
  </tr>
  
  <tr>
    <? if($act['RAKT_SIGNATURE'] == 'P') { ?>
    <td  colspan="3" style="text-align: center;"><?= text::get('EXPORT_FOOTER'); ?></td>
    <? } else { ?>
    <td  colspan="3" style="text-align: center;"><?= text::get('ELRCTRONIC_SIGNATURE'); ?></td>
    <? } ?>
  </tr>
</table>


</body>