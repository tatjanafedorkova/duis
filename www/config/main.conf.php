<?
# =============================================================================
# MySQL configuration
# =============================================================================

// MySQL: login
define ('MYSQL_USER','root');

// MySQL: password
define ('MYSQL_PASSWORD','');

// MySQL: host
define ('MYSQL_HOST','localhost');

// MySQL DB name
define ('MYSQL_DB','duistst');

# =============================================================================
# LDAP configuration
# =============================================================================

// The base dn for  domain
define ('LDAP_BASE_DIR','OU=LK,DC=energo,DC=lv');

// Array of domain controllers.
define ('LDAP_DOMAIN1','10.20.15.1');
define ('LDAP_DOMAIN2','10.20.15.2');

// The account suffix for domain
define ('LDAP_ACCOUNT_SUFFIX','@energo.lv');

// is test mode
define ('TEST_MODE', true);

// is LDAP autentificate mode
define ('LDAP_AUTH_MODE', false);

// list of system admins, autentification with out LDAP
define ('RCDIS_ADMIN_LIST', 'admin,ttest,tam,auditor');

# =============================================================================
# Main configuration
# =============================================================================

// Default sender email 
define('EMAIL_FROM', 'softex@softex.lv');

// ZR server url (with http:// or https://)
if(isset($_SERVER['SERVER_NAME'])) {
	define ('ZR_BASEURL','http'.((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on') ? 's' : '').'://'.$_SERVER['SERVER_NAME']);
} else {
	define ('ZR_BASEURL','http'.((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on') ? 's' : '').'://duis.energo.lv');
}

// Path on server
define ('ZR_SERVER_PATH',preg_replace('/config$/','',dirname(__FILE__)));
//define ('ZR_SERVER_PATH','..');

// LOG stored dir
define ('LOG_STORE_PATH', ZR_SERVER_PATH.'/logs');
// Files stored dir
define ('FILES_STORE_PATH', ZR_SERVER_PATH.'/files');
// GPS files stored dir
define ('GPS_FILES_STORE_PATH', ZR_SERVER_PATH.'/gps_files');
//  file stored dir
define ('EXCEL_FILES_STORE_PATH', ZR_SERVER_PATH.'/files/excel');
//  project file stored dir
define ('PROJECT_FILES_STORE_PATH', ZR_SERVER_PATH.'/excel');
//  xml file stored dir
define ('XML_FILES_STORE_PATH', ZR_SERVER_PATH.'/xml');
//  json file stored dir
define ('JSON_FILES_STORE_PATH', ZR_SERVER_PATH.'/json');

// Temp import files stored name
define ('IMPORT_FILES_STORE_NAME', 'RCD_TEMP.xls');

// Forms dir
define ('ZR_FORMS_DIR',ZR_SERVER_PATH.'/forms');

// Libs dir
define ('ZR_LIBS_DIR',ZR_SERVER_PATH.'/libs');

// Config dir
define ('ZR_CONF_DIR',ZR_SERVER_PATH.'/config');

// Temporary dir
define ('TEMP_DIR',ZR_SERVER_PATH.'/temp');

// session name	
define ('SESSION_NAME','rcd_session');

// cookie name for login
define ('COOKIE_LOGIN',SESSION_NAME.'_login');

// encrypt key name
define ('ENCRYPT_KEY','key');

// is debug mode
define ('DEBUG_MODE',true);

// current year
define ('CURRENT_YEAR','current_year');

// SOA service for KvikStep act status
define ('SOA_SERVISE_STATUS_TO_KVIKSTEP', 'http://dev.soa.energo.lv:8011/Jira/KVIKSTEP/REMStatusAPI');

// API for Limitkarte reports
define ('API_LIMITCARD_PROJRCT_MATERIAL', 'https://test.lews.energo.lv/ebs/v1/api/duis/get-project-material-report');
// API for nepabeigtu projektu reports
define ('API_UNCOMPLETED_PROJECT_REPORT', 'https://test.lews.energo.lv/ebs/v1/api/duis/get-uncompleted-pr-report');
// WS service for material request
// documentation https://test.lews.energo.lv/ebs/v1/api-docs/#/Duis/CreateRequisition
define ('MATERIAL_REQUEST_URL', 'https://test.lews.energo.lv/ebs/v1/api/duis/create-requisition');

// WS service for project Number request
// documentation https://test.lews.energo.lv/ebs/v1/api-docs/#/Duis/GetRequisitionStatus
define ('PRNUMBER_REQUEST_URL', 'https://test.lews.energo.lv/ebs/v1/api/duis/get-requisition-status');

# =============================================================================
# FMK configuration
# =============================================================================

// max length for showed text
define ('MAX_SHOW_TEXT_LENGTH',200);

# =============================================================================
# ADMIN USER ID
# =============================================================================

define ('MAIN_ADMIN_ID',197);

# =============================================================================
# GET variables name
# =============================================================================

// getting file - flag
define ('IS_GET_FILE_VARIABLE','get_file');

// getting excel file - flag
define ('IS_GET_EXCEL_FILE_VARIABLE','get_excel_file');

// getting project excel file - flag
define ('IS_GET_PEXCEL_FILE_VARIABLE','get_excel_file');

// getting gps file - flag
define ('IS_GET_GPS_FILE_VARIABLE','get_gps_file');

// getting xml file - flag
define ('IS_GET_XML_FILE_VARIABLE','get_xml_file');

// getting xml file - flag
define ('IS_GET_JSON_FILE_VARIABLE','get_json_file');

// get file variable name
define ('GET_FILE_VARIABLE','idFile');

// get file variable name
define ('GET_EXEL_FILE_VARIABLE','idFile');

// export file type
define('EXPORT_FILE_TYPE', 'application/pdf');

// export file extentions
define('EXPORT_FILE_EXTENTION', 'pdf');

// excel file mime type
define('EXCEL_FILE_MIMETYPE', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

// excel file extentions
define('EXCEL_FILE_EXTENTION', 'xlsx');

// project file type
define('PROJECT_FILE_TYPE', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

// xml file type
define('XML_FILE_MIMETYPE', 'text/xml');

// json file type
define('JSON_FILE_MIMETYPE', 'text/json');

// xml file extentions
define('XML_FILE_EXTENTION', 'xml');

// json file extentions
define('JSON_FILE_EXTENTION', 'json');

// form id 
define ('FORM_ID', 'formId');

// use or not global template
define ('DONT_USE_GLB_TPL','glb_tmp_off');

// popup mode
define ('POPUP_MODE','popup_mode');

// maksimalais excela rindu skaits
define ('MAX_EXCEL_ROW_COUNT',65500);

# =============================================================================
# urlQuery const
# =============================================================================

// is crypt?		
define ('URL_QUERY_IS_CRYPT',false);
// is hashing?		
define ('URL_QUERY_IS_HASH',false);

# =============================================================================
# Date interval
# =============================================================================
define ('NEXT_YEAR_START',20181227);
define ('PREV_YEAR_END',20190101);
// default datums
define ('DEFAULT_DATE', '2000-01-01 00:00:00');

# =============================================================================
# ST VV number
# =============================================================================
define ('ST_VV_NUMBER', '40003857687');


# =============================================================================
# Klasifikatori
# =============================================================================

define ('KL_USERS','KL_LIETOTAJI');
define ('KL_USERS_DU','KL_LIETOTAJI_DU');
define ('KL_HARMONIZED','KL_SASKANOTAJI');
define ('KL_ED_AREA','KL_ED_IECIRKNI');
define ('KL_ED_AREA_CODE','KL_ED_IECIRKNI_KODS');
define ('KL_ED_SECTIONS','KL_ED_NODALAS');
define ('KL_VOLTAGE','KL_SPRIEGUMS');
define ('KL_VOLTAGE_BY_CODE', 'KL_SPRIEGUMA_KODS');
define ('KL_VOLTAGE_TRASE','KL_SPRIEGUMS_TRASE');
define ('KL_WORK_OWNERS','KL_DARBA_UZNEMEJI');
define ('KL_ACT_TYPE','KL_AKTA_VEIDS');
define ('KL_ACT_TYPE_TRASE','KL_ACT_TYPE_TRASE');
define ('KL_ACT_TYPE_ALL','KL_ACT_TYPE_ALL');
define ('KL_OBJECTS','KL_OBJEKTI');
define ('KL_OBJECTS_BY_CODE', 'KL_OBJEKTA_KODS');
define ('KL_OBJECTS_TRASE','KL_OBJEKTI_TRASE');
define ('KL_CALCULALATION_GROUP','KL_KALKULACIJAS_GRUPAS');
define ('KL_CALCULALATION','KL_KALKULACIJA');
define ('KL_CALCULALATION_TR','KL_KALKULACIJA_TRASE');
define ('KL_MATERIALS','KL_MATERIALI');
define ('KL_MMS_WORKS','KL_MMS_DARBI');
define ('KL_WRITEOFF','KL_WRITEOFF_CODE');
define ('KL_REGIONS','KL_REGIONI');   
define ('KL_MMS_WORK_CALCULALATION','KL_MMS_KALKULACIJA');
define ('KL_CALCULALATION_MATERIAL','KL_KALKULACIJA_MATERIALI');
define ('KL_SYSTEM_PROCESS','KL_SISTEMAS_PROCESI');
define ('KL_WORK_TYPS','KL_DARBA_VEIDI');
define ('KL_CALC_NORM','KL_KALKULACIJAS_NORMAS');

# =============================================================================
# sistemas procesi kodes
# =============================================================================
define('PROC_KVIKSTEP_DUIS_STATUS', 'KVIKSTEP_DUIS_STATUS');

# =============================================================================
# KRFK constants
# =============================================================================

define('KRFK_REGION', 'REGION');
define('KRFK_ROLE', 'ROLE');
define('KRFK_STATUS', 'STATUS');
define('KRFK_IMPORT', 'IMPORTS');
define('KRFK_FAVORIT', 'FAVORITS');
define('KRFK_IMPORT_K', 'IMPORT');  
define('KRFK_POSITION', 'POSITION');

# =============================================================================
# ROLE
# =============================================================================

define ('ROLE_VIEWER', 'VIEWER');
define ('ROLE_ECONOMIST', 'ECONOMIST');
define ('ROLE_ED_USER', 'ED_USER');
define ('ROLE_AUDIT_USER', 'AUDITOR');
define ('ROLE_TR_USER', 'TRASE_EDIT'); 
define ('ROLE_PROJ_APPROVER', 'PROJ_APPROVER'); 

# =============================================================================
# FAVORIT KRFK ID
# =============================================================================

define ('KRFK_MATERIAL_ID', 20);
define ('KRFK_TRANSPORT_ID', 22);
define ('KRFK_CUSTOMER_ID', 21);
define ('KRFK_ED_AREA_ID', 23);
define ('KRFK_MMS_ID', 24);
define ('KRFK_CALCULATION_ID', 25);
define ('KRFK_MMS_ALL_ID', 28);

# =============================================================================
# status types
# =============================================================================

define('STAT_INSERT', 'INSERT');
define('STAT_ACCEPT', 'ACCEPT');
define('STAT_RETURN', 'RETURN');
define('STAT_CLOSE', 'CLOSE');
define('STAT_ESTIMATE', 'ESTIMATE');
define('STAT_DELETE', 'DELETE');
define('STAT_WRETEDOFF', 'CLOSEWRITEOFF');
define('STAT_TOPAY', 'TOPAY');
define('STAT_AUTO', 'AUTO');

# =============================================================================
# tabs
# =============================================================================

define('TAB_WORKS', 'TAB_WORKS');
define('TAB_MATERIAL', 'TAB_MATERIAL');
define('TAB_EXPORT', 'TAB_EXPORT');
define('TAB_AUDIT', 'TAB_AUDIT');
define('TAB_ESTIMATE', 'TAB_ESTIMATE');
define('TAB_WORKS_T', 'TAB_WORKS_T');
define('TAB_MATERIAL_T', 'TAB_MATERIAL_T');

# =============================================================================
# message types
# =============================================================================
define ('MSG_BUG',1);

# =============================================================================
# file types
# =============================================================================
define ('FILE_EXPORT','EXPORT');
define ('FILE_ESTIMATE','ESTIMATE');

# =============================================================================
# Operation code
# =============================================================================
// insert
define ('OP_INSERT','I');
// delete
define ('OP_DELETE','D');    
// update
define ('OP_UPDATE','U');
// update
define ('OP_EXPORT','E');
// update
define ('OP_ESTIMATE','T');
// return
define ('OP_RETURN','R');
// accept
define ('OP_ACCEPT','A');
// accept
define ('OP_COPY','C');
// search
define ('OP_SEARCH','S');
// nodot apmaksai
define ('OP_PAY','P');
// return  confirmed by admin
define ('OP_RETURN_CONFIRMED','X');
// delete  confirmed by admin
define ('OP_DELETE_CONFIRMED','Y');
// dzest rindu
define ('OP_DELETE_ROW', 'L');
// run job
define ('OP_JOB_RUN','J');
// kill job
define ('OP_JOB_KILL','K');
// straprezultāti
define ('OP_MIDLE_EXPORT', 'M');
// statusa maina no generets to ievads
define ('OP_CH_TO_INSERT', 'H');
// noraidit projektu, nomainīt statusu no ACCEPT to RETURN
define ('OP_DECLINE', 'N');
// eksportēt darbus un materialus
define ('OP_EXPORT_ALL','Z');
// Izveidot Materiālu pieprasījumu
define ('OP_ORDER_MATERIAL','O');

# =============================================================================
# USER ACTION CODES
# =============================================================================

define ('USER_AUTHEREISATION', 'USER_AUTHEREISATION');
define ('ACT_INSERT', 'ACT_INSERT');
define ('ACT_UPDATE', 'ACT_UPDATE');
define ('ACT_EXPORT', 'ACT_EXPORT');
define ('ACT_ESTIMATE', 'ACT_ESTIMATE');
define ('ACT_COPY', 'ACT_COPY');
define ('ACT_DELETE', 'ACT_DELETE');
define ('ACT_RETURN_INSERT', 'ACT_RETURN_INSERT');
define ('ACT_RETURN_ACCEPT', 'ACT_RETURN_ACCEPT');
define ('ACT_TOPAY', 'ACT_TOPAY');
define ('ACT_CLOSE', 'ACT_CLOSE');


# =============================================================================
# ACT TYPE ID
# =============================================================================

define ('ACT_TYPE_DAMAGE_ID', 10);
define ('ACT_TYPE_DEFECT_ID', 3);
define ('ACT_TYPE_STIHIJA_ID', 7);
define ('ACT_TYPE_DAMAGE_TR_ID', 6);
define ('ACT_TYPE_DEFECT_TR_ID', 3);
define ('ACT_TYPE_STIHIJA_TR_ID', 11);
define ('ACT_TYPE_PLAN_ID', 1);
define ('ACT_TYPE_PLAN_TR_ID', 9);

# =============================================================================
# PCT
# =============================================================================

define ('PCT_22', 22);
define ('PCT_21', 21);

# =============================================================================
# WORKER
# =============================================================================
define ('ST', 58);

# =============================================================================
# WORKER DELIVERY TYPE
# =============================================================================
define ('DU_DELIVERY_TYPE', 'DU_DELIVER');

# =============================================================================
# VOLTAGE
# =============================================================================

define ('VOLTAGE20KV', 7);
define ('VOLTAGE04KV', 6);

# =============================================================================
# OBJECT
# =============================================================================

define ('OBJECT20KV', 12);
define ('OBJECT04KV', 11);

#=================================================
#  priek? Eltel Networks AS
#=================================================
define('WORKER_CODE_ENA','10722319');
define('SECTION_CODE_ENA','31011,31012,31013,31014,31015,31016,31019');
define('CALC_GROUP_CODE_ENA','11,12,13,14,15,16,17,18,19');
define('WORKER_TAX_ENA','300000/13-207');
define('WORKER_SECTION_ENA','Daugavpils');

#=================================================
#  priek? Empower
#=================================================
define('WORKER_CODE_EPW','50003563201');
define('CALC_GROUP_CODE_EPW','21,22,23,24,25,26,27,28,29,31');
define('WORKER_TAX_EPW','300000/13-208');
define('WORKER_SECTION_EPW','Daugavpils,Preili,Ludza,Kraslava,Rezekne');

#=================================================
#  neatlasīt materialos
#=================================================
define('CAT_CLASE_1','Aprīkojums');
define('CAT_CLASE_2','Neprojektēt');

#=================================================
#  ST worker 
#=================================================
define('ST_WORKER_CODE','40003857687');

?>
