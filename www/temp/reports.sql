INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'REPORT_ACT_TOTAL', 'Darbu izpildes aktu kopsavilkums');
INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'REPORT_WORK_TOTAL', ' Darba pozīciju kopsavilkums');
INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'REPORT_MATERIAL_TOTAL', 'Akts par izlietotiem materiāliem');

INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'PCT', 'PVN');
INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'TOTAL_WITH_PCT', 'Kopā ar PVN');
INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'EXPORT_TO_PDF', 'Eksportēt uz PDF');

INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'UNIT_PRICE', 'Vienības cena');

INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'STOCK_NUMBER', 'Nomenklatūras nr.');

INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'ST_COMPANY_NAME', 'AS "Sadales tīkls"');

INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'TOATAL_MATERIAL_NAME', 'Nomenklatūras nosaukums');
INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'TOTAL_WORK_NAME', 'Kalkulācijas nosaukums');

INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'ACT_ACCEPT_TIME', 'Saskaņošanas datums');
INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'ACT_TOTAL_WORK_PRICE', 'Darbaspēka izmaksas');
INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'ACT_TOTAL_MATER_PRICE', 'DU materiālu izmaksas');
INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'ACT_TOTAL_PRICE', 'Kopējās izmaksas');

INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'REPORT_WORK_OFFER', 'Pasūtītājs');
INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'REPORT_WORKER', 'Izpildītājs');
INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'REPORT_AGREEMENT_NO', 'Vispārīgā vienošanās Nr.___________');
INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'REPORT_HEADER_1', 'Uzturēšanas remonta darbi');

INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'REPORT_REGION', 'reģions');
INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)
VALUES (NULL, 'REPORT_SECTION', 'nodaļa');

ALTER TABLE `search_criteria` ADD `SERCH_ID` INT( 11 ) NOT NULL AUTO_INCREMENT ,
ADD PRIMARY KEY ( `SERCH_ID` ) ;