INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)  VALUES (NULL, 'TO_IDN', 'TO_IDN');
INSERT INTO `FMK_MESSAGES` (`id`, `code`, `text`)  VALUES (NULL, 'TO_ID', 'TO_ID');

ALTER TABLE `AKTI` ADD `RAKT_TO_IDN` INT(12) DEFAULT 0 NULL;
ALTER TABLE `AKTI` ADD `RAKT_TO_ID` varchar(4)  NULL;

ALTER TABLE `KL_MMS_DARBI` ADD `KMSD_TO_IDN` INT(12) DEFAULT 0 NULL;
ALTER TABLE `KL_MMS_DARBI` ADD `KMSD_TO_ID` varchar(4)  NULL;

UPDATE `FMK_MESSAGES` SET `text` = '<table cellpadding="3" cellspacing="0" border="1" width="100%">
        <tr>
           <th>Kolonas dati</th>
           <th>Oblig�ts</th>
           <th>Kolonas datu apraksts</th>
        </tr>
        <tr>
            <td>Kods</td>
            <td>+</td>
            <td>MMS darba kods. Maksim�li 150 simboli. </td>
        </tr>
        <tr>
            <td>Cilvekstundas</td>
            <td>+</td>
            <td>Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>Materi�la izmaksas</td>
            <td>+</td>
            <td>Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>Ceturksnis</td>
            <td>+</td>
            <td>At�autas v�rt�bas: 1, 2, 3, 4. </td>
        </tr>
        <tr>
            <td>Gads</td>
            <td>+</td>
            <td>Cipars format� [xxxx]. </td>
        </tr>
        <tr>
            <td>Problemobjekts</td>
            <td>+</td>
            <td>At�autas v�rt�bas: 0, 1 </td>
        </tr>
        <tr>
            <td>Strukt�rvien�bas kods</td>
            <td>+</td>
            <td>Cipars format� [xxxx]. </td>
        </tr>
<tr>
            <td>Darbuz��m�js</td>
            <td>-</td>
            <td>Darbuz��m�ja identifikators. Cipars.</td>
        </tr>
        <tr>
            <td>L�guma Nr.</td>
            <td>+</td>
            <td>Visp�r�g�s vieno�an�s nr. Maksim�li 20 simboli.</td>
        </tr>
<tr>
            <td>DU trases</td>
            <td>+</td>
            <td>At�autas v�rt�bas: 0, 1 </td>
        </tr>
        <tr><td>1z</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>2z</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>3z</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>4z</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>5z</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>6z</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>7z</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>8z</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>9z</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>1v</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>2v</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>3v</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>4v</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>5v</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>6v</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>7v</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>8v</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>9v</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>10v</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr><td>11v</td><td>+</td><td>Cipars format� [xxxxx.xx].</td></tr>
        <tr>
            <td>Operat�vais apz�m�jums</td>
            <td>-</td>
            <td>Operat�vais apz�m�jums. Maksim�li 250 simboli. </td>
        </tr>
        <tr>
            <td>Darba nosaukums (izdruk�m)</td>
            <td>-</td>
            <td>Darba nosaukums (izdruk�m). Maksim�li 250 simboli. </td>
        </tr>
        <tr>
            <td>TO_IDN</td>
            <td>+</td>
            <td>Cipars format� [xxxxxxxxxxxx]. </td>
        </tr>
        <tr>
            <td>TO_ID</td>
            <td>-</td>
            <td> Maksim�li 4 simboli. </td>
        </tr>
       </table>'
WHERE `code` = 'REQUIREMENTS_MMS_WORKS';

ktl.14.inc
ktl.14.2.tpl
ktl.13.inc
dbProc.class
act.s.1.inc
act.s.2.tpl